const express = require('express')
const dotenv = require('dotenv')

const app = express()

dotenv.config()

app.get('', (req, res) => {
    res.status(200).json({
        message: "This is some lit code"
    })
})

app.listen(process.env.PORT, () => console.log(`App running on port ${process.env.PORT}`))